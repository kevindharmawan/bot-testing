package com.adprogc13.BotTesting.database;

import com.adprogc13.BotTesting.model.Expense;

import java.util.List;

public interface ExpenseDao {
    List<Expense> get();

    List<Expense> getByUserId(String userId);

    int saveExpense(String userId, String category, String timestamp, String nominal);
}
