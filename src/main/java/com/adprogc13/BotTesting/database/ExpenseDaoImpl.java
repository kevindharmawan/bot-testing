package com.adprogc13.BotTesting.database;

import com.adprogc13.BotTesting.model.Expense;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExpenseDaoImpl implements ExpenseDao {
    private JdbcTemplate jdbcTemplate;
    private static final String EXPENSE_TABLE = "tbl_expense";
    private static final String SQL_SELECT_ALL = "SELECT * FROM " + EXPENSE_TABLE;
    private static final ResultSetExtractor<List<Expense>>
            EXPENSE_EXTRACTOR = ExpenseDaoImpl::extractData;

    public ExpenseDaoImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public static List<Expense> extractData(ResultSet resultSet) throws SQLException {
        List<Expense> extractList = new ArrayList<Expense>();
        while (resultSet.next()) {
            Expense expense = new Expense(
                    resultSet.getString("user_id"),
                    resultSet.getString("category"),
                    resultSet.getString("timestamp"),
                    resultSet.getString("nominal"));
            extractList.add(expense);
        }
        return extractList;
    }

    @Override
    public List<Expense> get() {
        return jdbcTemplate.query(SQL_SELECT_ALL, EXPENSE_EXTRACTOR);
    }

    @Override
    public List<Expense> getByUserId(String userId) {
        String SQL_GET_BY_USER_ID = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
        return jdbcTemplate.query(SQL_GET_BY_USER_ID,
                new Object[]{"%" + userId + "%"},
                EXPENSE_EXTRACTOR);
    }

    @Override
    public int saveExpense(String userId, String category,
                           String timestamp, String nominal) {
        String REGISTER = "INSERT INTO " + EXPENSE_TABLE
                + " (user_id, category, timestamp, nominal) VALUES (?, ?, ?, ?);";
        System.out.println("ExpenseDAO saveExpense called!");
        System.out.println(REGISTER);
        return jdbcTemplate.update(REGISTER, userId, category, timestamp, nominal);
    }
}
