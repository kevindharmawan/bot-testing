package com.adprogc13.BotTesting.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
public class BotFlexMessage {
    @Autowired
    BotService botService;

    private final String encoding = StandardCharsets.UTF_8.name();

    public FlexMessage createFlexKategoriPengeluaran() {
        FlexMessage flexMessage = new FlexMessage("Kategori Atur Budget", null);

        try {
            ClassLoader classLoader = getClass().getClassLoader();

            String flexTemplate = IOUtils.toString(classLoader.getResourceAsStream("kategoriPengeluaran.json"));
            ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
            FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);

            flexMessage = new FlexMessage("Pilih Kategori", flexContainer);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return flexMessage;
    }
}
