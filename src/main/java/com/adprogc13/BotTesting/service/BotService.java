package com.adprogc13.BotTesting.service;

import com.adprogc13.BotTesting.handler.ResponseTemplate;
import com.adprogc13.BotTesting.handler.catatPengeluaran.PengeluaranKategoriHandler;
import com.adprogc13.BotTesting.repository.ExpenseDb;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

@Service
public class BotService {

    public Source source;

    private HashMap<String, ResponseTemplate> currentHandler = new HashMap<>();

    @Autowired
    BotFlexMessage botFlexMessage;

    @Autowired
    ExpenseDb expenseDb;

    @Autowired
    private LineMessagingClient lineMessagingClient;

    public void greetingMessage(String replyToken) {
        String senderId = source.getSenderId();
        currentHandler.put(senderId, null);
        replyFlexMenu(replyToken);
    }

    public void replyFlexMenu(String replyToken) {
        String senderId = source.getSenderId();
        UserProfileResponse sender = getProfile(senderId);
        replyText(replyToken, "Selamat datang!");
    }

    private void replyText(String replyToken, String message) {
        TextMessage textMessage = new TextMessage(message);
        reply(replyToken, textMessage);
    }

    public void reply(String replyToken, Message message) {
        ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
        reply(replyMessage);
    }

    private void reply(ReplyMessage replyMessage) {
        try {
            lineMessagingClient.replyMessage(replyMessage).get();
        } catch (InterruptedException | ExecutionException | RuntimeException e) {
            e.printStackTrace();
        }
    }

    public UserProfileResponse getProfile(String userId) {
        try {
            return lineMessagingClient.getProfile(userId).get();
        } catch (InterruptedException | ExecutionException | RuntimeException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void handleMessageEvent(MessageEvent messageEvent) {
        TextMessageContent textMessageContent = (TextMessageContent) messageEvent.getMessage();
        String replyToken = messageEvent.getReplyToken();
        String userMessage = textMessageContent.getText().toLowerCase();
        String senderId = source.getSenderId();

        if (!currentHandler.keySet().contains(senderId)) {
            currentHandler.put(senderId, null);
        }
        ResponseTemplate handler = currentHandler.get(senderId);
        if (handler != null) {
            currentHandler.put(senderId, handler.verificationMessage(userMessage));
            if (handler.getMessageToUser().equals("Pencatatan pengeluaran berhasil dilakukan")) {
                expenseDb.saveExpense(handler.getDescription());
            }
            replyText(replyToken, handler.getMessageToUser());
        }
        else {
            if (userMessage.equals("menu")) {
                replyText(replyToken, "Pesan masuk!");
            }
            else if (userMessage.equals("panjang")) {
                replyText(replyToken, ((Integer) expenseDb.getByUserId(senderId).size()).toString());
            }
            else if (userMessage.equals("catat pengeluaran")) {
                currentHandler.put(senderId, (ResponseTemplate) new PengeluaranKategoriHandler(senderId));
                reply(replyToken, botFlexMessage.createFlexKategoriPengeluaran());
            }
        }
    }
}
