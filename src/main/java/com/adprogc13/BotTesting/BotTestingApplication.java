package com.adprogc13.BotTesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotTestingApplication.class, args);
	}

}
