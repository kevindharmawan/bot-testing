package com.adprogc13.BotTesting.repository;

import com.adprogc13.BotTesting.database.ExpenseDao;
import com.adprogc13.BotTesting.model.Expense;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ExpenseDb {

    @Autowired
    private ExpenseDao expenseDao;

    public int saveExpense(String description) {
        String[] data = description.split(";");
        LocalDateTime timestamp = LocalDateTime.now().plusHours(7);
        return expenseDao.saveExpense(
                data[0],
                data[1],
                String.valueOf(timestamp),
                data[2]
        );
    }

    public List<Expense> getByUserId(String userId) {
        return expenseDao.getByUserId(userId);
    }
}
