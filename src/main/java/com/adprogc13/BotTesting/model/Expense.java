package com.adprogc13.BotTesting.model;

public class Expense {
    private String userId;
    private String category;
    private String timestamp;
    private String nominal;

    public Expense(String userId, String category, String timestamp, String nominal) {
        this.userId = userId;
        this.category = category;
        this.timestamp = timestamp;
        this.nominal = nominal;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return this.category;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getNominal() {
        return this.nominal;
    }
}
