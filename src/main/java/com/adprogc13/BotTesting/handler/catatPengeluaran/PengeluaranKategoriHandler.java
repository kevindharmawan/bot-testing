package com.adprogc13.BotTesting.handler.catatPengeluaran;

import com.adprogc13.BotTesting.handler.ResponseTemplate;

import java.util.ArrayList;
import java.util.List;

public class PengeluaranKategoriHandler extends ResponseTemplate {
    private static final List<String> kategoriPengeluaran = new ArrayList<String>() {
        {
            add("konsumsi");
            add("transportasi");
            add("utilitas");
            add("belanja");
            add("lainnya");
        }
    };

    public PengeluaranKategoriHandler(String userId) {
        this.description = userId;
    }

    @Override
    public ResponseTemplate verificationMessage(String message) {
        if (kategoriPengeluaran.contains(message)) {
            return handle(message);
        } else {
            return cancelOperation(message);
        }
    }

    @Override
    public ResponseTemplate handle(String message) {
        description += ";" + message;
        messageToUser = "Kategori " + message + " berhasil terpilih."
                + " Berapa jumlah uang yang ingin kamu masukkan ke catatan pengeluaran?"
                + " Contoh: 50000";
        return new PengeluaranNominalHandler(this);
    }

    @Override
    public ResponseTemplate unknownMessage() {
        messageToUser = "Kategori yang dipilih tidak ada"
                + ", tolong pilih salah satu dari kategori yang tersedia."
                + " Ketik 'Batal' jika ingin membatalkan.";
        return this;
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}
