package com.adprogc13.BotTesting.handler.catatPengeluaran;

import com.adprogc13.BotTesting.handler.ResponseTemplate;

public class PengeluaranKonfirmasiHandler extends ResponseTemplate {

    public PengeluaranKonfirmasiHandler(PengeluaranNominalHandler state) {
        this.state = state;
    }

    @Override
    public ResponseTemplate verificationMessage(String message) {
        if (message.equalsIgnoreCase("ya")) {
            return handle(message);
        } else {
            return cancelOperation(message);
        }
    }

    @Override
    public ResponseTemplate handle(String message) {
        messageToUser = "Pencatatan pengeluaran berhasil dilakukan";
        description = "";
        return null;
    }

    @Override
    public ResponseTemplate unknownMessage() {
        messageToUser = "Untuk konfirmasi pencatatan jawab 'Ya'"
                + " dan untuk pembatalan pencatatan jawab 'Batal'.";
        return this;
    }

    @Override
    public String getDescription() {
        return state.getDescription() + ";" + description;
    }
}