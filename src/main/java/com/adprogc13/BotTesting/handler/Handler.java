package com.adprogc13.BotTesting.handler;

public interface Handler {
    ResponseTemplate verificationMessage(String message);

    ResponseTemplate handle(String message);

    String getDescription();
}
