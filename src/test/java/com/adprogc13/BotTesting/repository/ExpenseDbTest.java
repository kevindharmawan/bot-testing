package com.adprogc13.BotTesting.repository;

import com.adprogc13.BotTesting.database.ExpenseDao;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseDbTest {

    @Mock
    private ExpenseDao expenseDao;

    @InjectMocks
    private ExpenseDb expenseDb;

    @Test
    void testExpenseGetByUserIdTest() {
        when(expenseDao.getByUserId("userid")).thenReturn(new ArrayList<>());
        Assert.assertTrue(expenseDb.getByUserId("userid") instanceof ArrayList);
    }

    @Test
    void testExpenseSave() {
        when(expenseDao.saveExpense(
                eq("userid"),
                eq("category"),
                anyString(),
                eq("nominal")
        )).thenReturn(1);
        String description = "userid;category;nominal";
        Assert.assertEquals(1, expenseDb.saveExpense(description));
    }
}
